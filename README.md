# Teste Madeinweb: Front-End
Desenvolver uma aplicação HTML5

## Instruções
- Faça um fork desse projeto para a sua conta pessoal do GitHub, ou BitBucket.
- Siga as especificações abaixo.
- Crie um README com as instruções para compilar, testar e rodar o projeto.
- O link do repositório deverá ser enviado para o e-mail rh@madeinweb.com.br com o título **Teste FrontEnd**

## Especificações tecnicas
- Seguir este layout:
- Utilizar a [API de busca do YouTube](https://developers.google.com/youtube/v3/docs/search/list)
- Mobile first e responsivo
- Cores livres, layout livre, imagens livres
- Gitflow
- **Importante:** A url da página não pode ser recarregada em momento algum.
- Layout web: [clique aqui](https://projects.invisionapp.com/share/SYD0CSUFP)
- Layout mobile: [clique aqui](https://projects.invisionapp.com/share/WKD0CYIRG#/screens/248337735_iPhone_7)
- Você pode baixar as specs do projeto  [clicando aqui](https://drive.google.com/a/madeinweb.com.br/file/d/0B6TTk9-0XqYXN2cwbTFZM0hseWc/view?usp=sharing).
* A pasta "Teste_made_mobile_specs" e a pasta "Teste_made_web_spec" contém um arquivo "index.html", que pode ser usado para verificar as propriedades de cada elemento do layout. As imagens em png (Logo e ícone de voltar) podem ser baixadas, ao clicar nas imagens, é possível acessar o arquivo .png do lado direito ou através da segunda aba do canto superior esquerdo.

## Especificações funcionais
### Tela Inicial
Essa tela terá um formulário de busca posicionado no meio da tela com campo de texto com placeholder "Pesquisar" e um botão "Buscar". Esse formulário deverá ter validação.

Essa busca deverá chamar a url https://www.googleapis.com/youtube/v3/search?part=id,snippet&q={termo_de_busca}&key={API_KEY}

Ao fazer a busca, o formulário deve ser movido para o topo da tela usando css animate e mostrar a lista de resultados com os campos título, descrição, thumbnail e um link para a página de detalhes. As sugestões de animações a ser utilizada estão no ZIP, na versão web com nome "teste_web.mov" e versão mobile "Video_mobile.mov".



### Tela de detalhes
A partir do videoId retornado na outra chamada, deve ser feito uma chamada para https://www.googleapis.com/youtube/v3/videos?id={VIDEO_ID}&part=snippet,statistics&key={API_KEY}

A partir desse retorno, deve-se montar uma tela contendo embed do video, título, descrição e visualizações.

Essa tela deve ter um botão para voltar para resultados da busca.

### Breakpoints:

| Nome do breakpoint | Largura mínima | Descrição                         |
|--------------------|----------------|-----------------------------------|
| phone              | 320px          | Breakpoint para smartphones       |
| tablet             | 768px          | Breakpoint para tablets           |
| desktop            | 1024px         | Breakpoint para desktops comuns   |
| monitor            | 1280px         | Breakpoints para desktops grandes |

## O que será avaliado?
- Organização do projeto
- Lógica do código
- Uso do Git
- Qualidade visual (layout, animações utilizadas)

## Pontos Extras:
* Utilizar algum pré-processador CSS, preferencialmente SASS
* Testes automatizados :)
* Automatizar as coisas. (Gulp/Grunt/webpack)
* Utilizar animações para o scroll da página
* Fazer paginação na tela de vídeos, utilizando os [recursos de paginação da api](https://developers.google.com/youtube/v3/guides/implementation/pagination?hl=pt-br).